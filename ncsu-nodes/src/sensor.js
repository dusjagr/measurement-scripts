const fs = require("fs");
const moment = require("moment");

// Set up Serial Port to connect to device.  
serialConfig = {
  port: "/dev/ttyACM0", // Linux (typical) check location for Windows or Mac
  baudRate: 115200,
  dataBits: 8,
  stopBits: 1,
  parity: "none"
};

// Location where we are putting the raw serial input from devcie
input = fs.readFileSync("./data/raw.txt", "utf8");

serial = require("./lib/serial.js").Serial;
app = require("./lib/app.js").App;
utils = require("./lib/utils.js");

// Feed incoming serial information to 'input' location defined above
serial.feed(input);





http = require('http');

var soil_data = '';

http.get("https://rest.soilgrids.org/query?lon=5.39&lat=51.57", (resp) => {
//    var data = '';    

    resp.on('data', (chunk) => {
        soil_data += chunk;
    });

    resp.on('end', () => {
//        console.log(data);
        ui.info("response", soil_data)
        app.result(soil_data)
    });
}).on("error", (err) => {
  app.result("error")
  console.log("Error: " + err.message);
});

var r = {
    "status": "success"
}

ui.info("do it", soil_data)









// Get serial data from device.  At the same time, parse the lines that we know are useful data.
async function main() {
  var next = false;

  var lines = [];
  await utils.sleep(1000);
  console.log("reading input");
  for (;;) {
    r = await serial.readLine();
    console.log(r);

    if (next && r.trim() != "") {
      lines.push(r);
      next = false;
    }

    if (r.startsWith("   For CS655: ")) {
      next = true;
    }

    if (r == "Exit") {
      break;
    }
  }

  console.log(lines);

  res = {};

// Parse the data lines we pulled from Serial based on the column names.  
// This would be easier if the information came as a valid JSON from the get go :)
  for (var i = 0; i < lines.length; i++) {
    // res.$nodeid => 2D array
    // columns: $timestamp, bat V, sensor addr, residue temp, %vwc, soil temp, permittivity, bulk EC
    var l = lines[i].split(",");
    if (l.length != 11) {
      console.log("ignoring CS655");
      continue;
    }

    var nodeId = l[0];

    if (!(nodeId in res)) {
      res[nodeId] = [];
    }

    res[nodeId].push([moment(l[2], "M/D/YYYY H:m"), l[1], l[3], l[4], l[5], l[6], l[7], l[8], l[9]]);
  }

  res["raw"] = input;

  app.result(res);
}

main();
