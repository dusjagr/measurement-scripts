DesktopApp = {
    result: function (res) {
        var tmp = JSON.stringify(res, null, 2);
        console.log("saving result to ./data/result.json:\n" + tmp);
        var fs = require('fs');
        if (!fs.existsSync("./data")) {
            fs.mkdirSync("./data");
        }
        fs.writeFileSync("./data/result.json", tmp);
        process.exit(0);
    },
    progress: function (pct) {
        console.log("progress is " + pct + " %");
    },
    onSerialDataIntercepted: function (data) {
        console.log("serial data intercepted: " + data);
    },
    csvExport: function (key, value) {
        console.log("exporting key value pair to csv: " + key + " => " + value);
    },
    csvExportValue: function (value) {
        console.log("exporting value to CSV " + value);
    },
    getAnswer: function (dataName) {
        console.log("get data name " + dataName)
        return "example value";
    },
    save: function () {
        console.log("saving env")
        return null;
    }
}

AndroidApp = {
    result: function (res) {
        var tmp = JSON.stringify(res, null, 2);
        android.result(tmp);
    },
    progress: function (pct) {
        android.progress(pct)
    },
    onSerialDataIntercepted: function (data) {
        android.onSerialDataIntercepted(data);
    },
    csvExport: function (key, value) {
        android.csvExport(key, value);
    },
    csvExportValue: function (value) {
        android.csvExport(value);
    },
    getAnswer: function (dataName) {
        return android.getAnswer(dataName);
    },
    save: function () {
        return android.save();
    }
}


App = null;
if (typeof _ANDROID === 'undefined') {
    App = DesktopApp;
} else {
    App = AndroidApp;
}
module.exports.App = App