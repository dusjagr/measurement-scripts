/* global sensor */
import * as utils from './utils';

const oboe = require('oboe');
const EventEmitter = require('events');

let parser = null;
let buffer = '';
let port;

const serialDataEmitter = new EventEmitter();

const BaseSerial = {
  read() {
    return new Promise(resolve =>
      serialDataEmitter.on('data', (data) => {
        resolve(data);
      }));
  },
  readLine: async () =>
    new Promise((resolve) => {
      serialDataEmitter.on('data', (data) => {
        buffer += data;
        const idx = buffer.indexOf('\n');
        if (idx >= 0) {
          resolve(buffer.substring(0, idx));
          if (buffer.length > idx) {
            buffer = buffer.substring(idx + 1, buffer.length);
          } else {
            buffer = '';
          }
        }
      });

      if (buffer) {
        const tmp = buffer;
        buffer = '';
        serialDataEmitter.emit('data', tmp);
      }
    }),
  async readJson() {
    return new Promise((resolve, reject) => {
      parser = oboe();

      const timeout = setTimeout(() => {
        reject(new Error('timeout'));
      }, 2147483647);

      parser.on('done', (json) => {
        parser.abort();
        parser = null;
        clearTimeout(timeout);
        resolve(json);
      });

      parser.on('fail', (error) => {
        parser = null;
        // console.log(JSON.stringify(error, null, 2));
        clearTimeout(timeout);
        reject(error);
      });

      if (buffer) {
        parser.emit('data', buffer);
        buffer = '';
      }
    });
  },
  readStreamingJson: (pattern, callback) =>
    new Promise((resolve, reject) => {
      parser = oboe();

      const timeout = setTimeout(() => {
        reject(new Error('timeout'));
      }, 2147483647);

      if (pattern != null && callback != null) {
        console.log(`listening for node ${pattern}`);
        parser.node(pattern, (data) => {
          callback(data);
        });
      }

      parser.on('done', (json) => {
        console.log('on parser done');
        parser.abort();
        parser = null;
        console.log('resolving');
        clearTimeout(timeout);
        resolve(json);
      });

      parser.on('fail', (error) => {
        parser = null;
        console.log(`rejecting: ${error}`);
        clearTimeout(timeout);
        reject(error);
      });

      if (buffer) {
        parser.emit('data', buffer);
        buffer = '';
      }
    }),
  feed() {},
};

const AndroidSerial = {
  init() {},
  read: BaseSerial.read,
  readLine: BaseSerial.readLine,
  readJson: BaseSerial.readJson,
  readStreamingJson: BaseSerial.readStreamingJson,
  write(data) {
    sensor.serialWrite(data);
  },
  feed: BaseSerial.feed,
};

const DesktopSerial = {
  async init(device, {
    baudRate = 115200, dataBits = 8, stopBits = 1, parity = 'none',
  }) {
    const SerialPort = require('serialport');

    port = new SerialPort(
      device,
      {
        baudRate,
        dataBits,
        stopBits,
        parity,
      },
      (err) => {
        if (err != null) {
          console.log(err);
          port = null;
        }
      },
    );

    if (!port) {
      return;
    }

    port.pipe(new SerialPort.parsers.Readline());

    try {
      port.on('data', (data) => {
        console.log('received data');
        if (parser != null) {
          parser.emit('data', `${data}`);
        } else {
          serialDataEmitter.emit('data', data);
        }
      });
    } catch (e) {
      console.log(e);
    }

    await utils.sleep(2000);
  },
  read: BaseSerial.read,
  readLine: BaseSerial.readLine,
  readJson: BaseSerial.readJson,
  readStreamingJson: BaseSerial.readStreamingJson,
  write(data) {
    if (typeof port === 'undefined') {
      console.log(`skipping write: ${data}`);
      return;
    }

    try {
      port.write(data);
    } catch (err) {
      console.error(`unable to write to serial port: ${err}`);
    }
  },
  feed(filename) {
    const fs = require('fs');
    const mock = fs.readFileSync(filename);

    if (parser != null) {
      parser.emit('data', mock);
    } else {
      buffer += `${mock}`;
    }
    console.log(`buffer: ${buffer}`);
  },
};

const Serial = (() => {
  if (typeof _ANDROID === 'undefined') {
    return DesktopSerial;
  }
  return AndroidSerial;
})();

Serial.onDataAvailable = () => {
  // this is only called by Android

  if (parser != null) {
    parser.emit('data', sensor.serialRead());
  } else {
    serialDataEmitter.emit('data', sensor.serialRead());
  }
};

export default Serial;
