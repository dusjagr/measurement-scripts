/**
 * This is a simple serial sensor script
 * The firmware for the corresponding sensor can be found
 * in arduino-sketch/arduino-sketch.ino
 *
 * See the tutorial over here
 * https://gitlab.com/our-sci/measurement-scripts/tree/master/hello-world
 */

import app from './lib/app';
import serial from './lib/serial';
// import { sleep } from './lib/utils'; // use: await sleep(1000);

export default serial; // expose this to android for calling onDataAvailable

(async () => {
  await serial.init('/dev/ttyACM0', {
    baudRate: 115200,
    dataBits: 8,
    stopBits: 1,
    parity: 'none',
  });

  // instead of reading from the serial port mock some data
  // serial.feed('./mock/mock_sensor_output.json');

  console.log('sending a');
  serial.write('a');

  try {
    const res = await serial.readJson();
    app.progress(100);

    console.log(`res ${JSON.stringify(res)}`);
    app.result(res);
  } catch (err) {
    console.error(`error reading json: ${err}`);
  }
})();
